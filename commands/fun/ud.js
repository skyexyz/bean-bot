const { RichEmbed } = require('discord.js');
const { Command } = require('discord.js-commando');
const fetch = require('node-fetch');

module.exports = class HelpCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'ud',
            group: 'fun',
            memberName: 'ud',
            description: 'Fetches a definition from urbandictionary.com',
            guarded: true,
            args: [
                {
                    key: 'word',
                    prompt: 'What word do you want to look up?',
                    type: 'string',
                    default: ''
                },
            ]
        });
    }

    async run(msg, { word }) {
        let fetchMsg = null;
        try {
            fetchMsg = await msg.channel.send('_fetching definition..._');
            const response = await fetch('https://api.urbandictionary.com/v0/define?term=' + word);
            const json = await response.json();
            const definition = json.list[0].definition;

            if (definition) {
                const embeded = new RichEmbed()
                    .setTitle('Urban Dictionary: ' + word)
                    .setColor(0x0E2158)
                    .setDescription(definition);
                fetchMsg.delete();

                return msg.channel.send(embeded);
            }

            if (fetchMsg !== null) {
                fetchMsg.delete();
            }

            return msg.say('Something went wrong while fetching the urban dictionary definiton :(');
        } catch (ex) {
            if (fetchMsg !== null) {
                fetchMsg.delete();
            }
            console.error(ex);
            return msg.say('Something went wrong while fetching the urban dictionary definiton :(');
        }
    }
};
