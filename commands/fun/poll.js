const { MessageEmbed, RichEmbed } = require('discord.js');
const { Command } = require('discord.js-commando');
const { stripIndents } = require('common-tags');

module.exports = class HelpCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'poll',
            group: 'fun',
            memberName: 'poll',
            description: 'Creates a poll',
            guarded: true,
            args: [
                {
                    key: 'title',
                    prompt: 'What is your poll about?',
                    type: 'string',
                    default: ''
                },
                {
                    key: 'react1',
                    prompt: 'What emote should be used as the first reaction?',
                    type: 'string',
                    default: '👍'
                },
                {
                    key: 'react2',
                    prompt: 'What emote should be used as the second reaction?',
                    type: 'string',
                    default: '👎'
                },
            ]
        });
    }

    async run(msg, { title, react1, react2 }) {
        const embed = new RichEmbed()
            .setTitle(title)
            .setFooter('Poll created by ' + msg.author.username)
            .setColor(0x00AE86);
        
        let reply = await msg.channel.send(embed);

        await reply.react(react1);
        await reply.react(react2);
    }
};
