if  (process.env.NODE_ENV ===  'development') {
    require('dotenv').config();
}
const { BOT_TOKEN, BOT_OWNERS, SENTRY_DSN } = process.env;
const path = require('path');
const Commando = require('discord.js-commando');
const Keyv = require('keyv');
const KeyvProvider = require('commando-provider-keyv');
const Sentry = require('@sentry/node');


Sentry.init({ dsn: SENTRY_DSN });

const client = new Commando.Client({
    owner: BOT_OWNERS.split(','),
    unknownCommandResponse: false,
});

var keyv = new Keyv();
if (process.env.KEYV_STORAGE_URI !== undefined) {
    console.log('Keyv storage initialized')
    keyv = new Keyv(process.env.KEYV_STORAGE_URI);
}
client.setProvider(new KeyvProvider(keyv));

client.registry
    .registerDefaultTypes()
    .registerGroups([
        ['util', 'Utility'],
        ['fun', 'Fun'],
    ])
    .registerDefaultCommands({
        help: false,
        ping: false,
        prefix: true,
        commandState: false,
        unknownCommand: false,
        eval_: false,
    })
    .registerCommandsIn(path.join(__dirname, 'commands'));

client.on('guildMemberAdd', async member => {
    //await sendSystemMessage(member, 'welcomeMessage', '{{user}} has joined the server!');
});

client.on('guildMemberRemove', async member => {
    //await sendSystemMessage(member, 'leaveMessage', '{{user}} has left');
});

async function sendSystemMessage(member, type, defaultMessage) {
    const channel = member.guild.systemChannel;
    if (!channel || !channel.permissionsFor(client.user).has('SEND_MESSAGES')) return null;
    try {
        const leaveMsg = await client.provider.get(member.guild, type, defaultMessage);
        await channel.send(leaveMsg.replace(/{{user}}/gi, `**${member.user.tag}**`));
        return null;
    } catch (err) {
        console.error(err);
        return null;
    }
}

client.login(BOT_TOKEN);