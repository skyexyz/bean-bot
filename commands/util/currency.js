const { Command } = require('discord.js-commando');
const { FIXER_API_KEY } = process.env;
const money = require('money');
const fetch = require('node-fetch');
const Sentry = require('@sentry/node');

module.exports = class HelpCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'currency',
            aliases: ['c'],
            group: 'util',
            memberName: 'currency',
            description: 'Convert from one currency to another',
            guarded: true
        });
    }

    async run(msg) {
        var args = msg.argString.trim().match(/(\d+).*([\w]{3}).*([\w]{3})/);
        if (args === null || args.length !== 4) {
            msg.reply("Invalid usage of command");

            return;
        }
        let amount = args[1].toUpperCase();
        let from = args[2].toUpperCase();
        let to = args[3].toUpperCase();

        let fixer = await this.client.provider.get('global', 'fixerRates');
        let fixerTimestamp = 0;
        if (fixer !== undefined) {
            fixerTimestamp = new Date(fixer.timestamp*1000);
            fixerTimestamp.setHours(fixerTimestamp.getHours() + 1);
        }
        if (fixer === undefined || fixerTimestamp < Date.now()) {
            try {
                let fixerResponse
                    = await fetch(`http://data.fixer.io/api/latest?access_key=${FIXER_API_KEY}&format=1`);
                fixer = await fixerResponse.json();
                this.client.provider.set('global', 'fixerRates', fixer);
            } catch (ex) {
                Sentry.captureException(ex);
            }
        }
        money.rates = fixer.rates;

        let result = money(amount).from(from).to(to);
        result = parseFloat(result).toFixed(2);

        msg.reply(`${amount}${from} = ${result}${to}`);
    }
};
