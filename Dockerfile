FROM node:12-alpine

ENV BOT_TOKEN="" \
    BOT_OWNERS="" \
    KEYV_STORAGE_URI="" \
    FIXER_API_KEY="" \
    SENTRY_DSN=""

WORKDIR /app

COPY package*.json ./

RUN apk --no-cache add pango pango-dev jpeg-dev build-base wget fontconfig giflib-dev \
    && npm install \
    && apk del build-base \
    && mkdir -p /usr/share/fonts/truetype/bot-fonts \
    && wget https://github.com/tsenart/sight/raw/master/fonts/Consolas.ttf?raw=true -O/usr/share/fonts/truetype/bot-fonts/Consolas.ttf \
    && wget https://github.com/sophilabs/macgifer/blob/master/static/font/impact.ttf?raw=true -O/usr/share/fonts/truetype/bot-fonts/Impact.ttf \
    && fc-cache -f

COPY . .

CMD ["node","index.js"]
