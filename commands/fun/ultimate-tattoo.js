const { RichEmbed } = require('discord.js');
const { Command } = require('discord.js-commando');
const { createCanvas, loadImage } = require('canvas');
const fetch = require('node-fetch');
const path = require('path');

module.exports = class HelpCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'ultimate-tattoo',
            group: 'fun',
            memberName: 'ultimate-tattoo',
            description: 'Draws a users avatar as the ultimate tattoo',
            guarded: true,
            args: [
                {
                    key: 'user',
                    prompt: 'User',
                    type: 'user',
                    default: ''
                },
            ]
        });
    }

    async run(msg, { user }) {
        let user1Url = user.displayAvatarURL;
        try {
            let base  = await loadImage(path.join(__dirname, '..', '..', 'resources', 'memes', 'ultimate-tattoo.png'));
            let avatar = await loadImage(user1Url);

            const canvas = createCanvas(base.width, base.height);
            const ctx = canvas.getContext('2d');
            ctx.drawImage(base, 0, 0);
            ctx.rotate(-10 * (Math.PI / 180));
            ctx.drawImage(avatar, 84, 690, 300, 300);
            ctx.rotate(10 * (Math.PI / 180));
            
            return msg.say({ files: [{ attachment: canvas.toBuffer(), name: 'ultimate-tattoo.png' }] });
        } catch(ex) {
            console.error(ex);
        }
    }
};
