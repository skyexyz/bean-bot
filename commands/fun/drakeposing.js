const { RichEmbed } = require('discord.js');
const { Command } = require('discord.js-commando');
const { createCanvas, loadImage } = require('canvas');
const fetch = require('node-fetch');
const path = require('path');

module.exports = class HelpCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'drakeposing',
            group: 'fun',
            memberName: 'drakeposing',
            description: 'Creates a drake meme based on two users',
            guarded: true,
            args: [
                {
                    key: 'user1',
                    prompt: 'User 1',
                    type: 'user',
                    default: ''
                },
                {
                    key: 'user2',
                    prompt: 'User 2',
                    type: 'user',
                    default: ''
                }
            ]
        });
    }

    async run(msg, { user1, user2 }) {
        let user1Url = user1.displayAvatarURL;
        let user2Url = user2.displayAvatarURL;
        try {
            let base  = await loadImage(path.join(__dirname, '..', '..', 'resources', 'memes', 'drakeposing.jpg'));
            let user1Avatar = await loadImage(user1Url);
            let user2Avatar = await loadImage(user2Url);
            
            let canvas = createCanvas(base.width, base.height);
            let ctx =  canvas.getContext('2d');
            ctx.drawImage(base, 0, 0);
            ctx.drawImage(user1Avatar, 153, 0, 145, 145);
            ctx.drawImage(user2Avatar, 153, 147, 145, 145);
            
            return msg.say({ files:  [{attachment: canvas.toBuffer(), name: 'drakeposing.png'}]});
        } catch(ex) {
            console.error(ex);
        }
    }
};
