const { RichEmbed } = require('discord.js');
const { Command } = require('discord.js-commando');
const { createCanvas, loadImage } = require('canvas');
const fetch = require('node-fetch');
const path = require('path');

module.exports = class HelpCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'shit',
            group: 'fun',
            memberName: 'shit',
            description: 'Creates a I stepped in shit meme',
            guarded: true,
            args: [
                {
                    key: 'user',
                    prompt: 'User',
                    type: 'user',
                    default: ''
                },
            ]
        });
    }

    async run(msg, { user }) {
        let user1Url = user.displayAvatarURL;
        try {
            let base  = await loadImage(path.join(__dirname, '..', '..', 'resources', 'memes', 'steppedinshit.jpg'));
            let user1Avatar = await loadImage(user1Url);

            let canvas = createCanvas(base.width, base.height);
            let ctx =  canvas.getContext('2d');
            ctx.drawImage(base, 0, 0);
            ctx.drawImage(user1Avatar, 275, 850, 250, 250);

            return msg.say({ files:  [{attachment: canvas.toBuffer(), name: 'steppedinshit.png'}]});
        } catch(ex) {
            console.error(ex);
        }
    }
};
