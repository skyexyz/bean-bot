const { RichEmbed } = require('discord.js');
const { Command } = require('discord.js-commando');
const { createCanvas, loadImage } = require('canvas');
const fetch = require('node-fetch');
const path = require('path');

module.exports = class HelpCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'isthisa',
            group: 'fun',
            memberName: 'isthisa',
            description: 'Creates a "Is this a pidgeon?" style meme',
            guarded: true,
            args: [
                {
                    key: 'user',
                    prompt: 'Which user is this about?',
                    type: 'user',
                    default: ''
                },
                {
                    key: 'text',
                    prompt: 'Use a complete setence "Is this a vegan?"',
                    type: 'string',
                    default: ''
                },
            ]
        });
    }

    async run(msg, { user, text }) {
        let user1Url = user.displayAvatarURL;
        try {
            let base  = await loadImage(path.join(__dirname, '..', '..', 'resources', 'memes', 'isthisapidgeon.jpg'));
            let avatar = await loadImage(user1Url);

            const canvas = createCanvas(base.width, base.height);
            const ctx = canvas.getContext('2d');
            ctx.drawImage(base, 0, 0);
            ctx.drawImage(avatar, 807, 41, 200, 200);
            ctx.font = "70px Impact";
            ctx.textAlign = "center";
            ctx.fillStyle = "white";
            ctx.fillText(text, base.width/2, 600, 1000);
            ctx.strokeStyle = "black";
            ctx.lineWidth = 3;
            ctx.strokeText(text, base.width/2, 600, 1000);
            
            return msg.say({ files: [{ attachment: canvas.toBuffer(), name: 'isthisa.jpg' }] });
        } catch(ex) {
            console.error(ex);
        }
    }
};
