const { Command } = require('discord.js-commando');

module.exports = class HelpCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'mock',
            group: 'fun',
            memberName: 'mock',
            description: 'mock text',
            guarded: true,
            args: [
                {
                    key: 'text',
                    prompt: 'Text to mock',
                    type: 'string',
                    default: ''
                }
            ]
        });
    }

    async run(msg, { text }) {
        text = text.toLowerCase();
        let mockText = "";
        for (let i = 0; i < text.length; i++) {
            if (i % 2 === 1) {
                mockText += text.charAt(i).toUpperCase();
            } else {
                mockText += text.charAt(i);
            }
        }

        msg.channel.send(mockText);
    }
};
