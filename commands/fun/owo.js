const { Command } = require('discord.js-commando');
const owoify = require('owoify-js').default;

module.exports = class HelpCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'owo',
            group: 'fun',
            memberName: 'owo',
            description: 'Owoify your text',
            guarded: true,
            args: [
                {
                    key: 'text',
                    prompt: 'Text to be owoified',
                    type: 'string',
                    default: ''
                }
            ]
        });
    }

    async run(msg, { text }) {
        msg.channel.send(owoify(text, 'uwu').replace('`', '\`'));
    }
};
