const { Command } = require('discord.js-commando');
const owoify = require('owoify-js').default;

module.exports = class HelpCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'uvu',
            group: 'fun',
            memberName: 'uvu',
            description: 'Uvuify your text',
            guarded: true,
            args: [
                {
                    key: 'text',
                    prompt: 'Text to be uvuified',
                    type: 'string',
                    default: ''
                }
            ]
        });
    }

    async run(msg, { text }) {
        msg.channel.send(owoify(text, 'uvu').replace('`', '\`'));
    }
};
