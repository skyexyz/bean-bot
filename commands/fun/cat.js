const { Command } = require('discord.js-commando');
const fetch = require('node-fetch');

module.exports = class HelpCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'cat',
            group: 'fun',
            memberName: 'cat',
            description: 'Shows a random picture of a cat',
            guarded: true,
        });
    }

    async run(msg) {
        try {
            let catFactResponse = await fetch('https://some-random-api.ml/facts/cat');
            let catFact = catFactResponse.json().fact;
            let cat = await fetch('https://cataas.com/cat');
            let catBuffer = await cat.buffer();

            return msg.say(catFact, { files:  [{attachment: catBuffer, name: 'cat.png'}]});
        } catch(ex) {
            console.error(ex);
        }
    }
};
