const { Command } = require('discord.js-commando');
const { createCanvas } = require('canvas');
const GIFEncoder = require('gifencoder');
const streamBuffers = require('stream-buffers');

const SIZE = 64;
module.exports = class HelpCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'scrolling-text',
            group: 'fun',
            memberName: 'scrolling-text',
            description: 'Creates a scrolling text emote',
            guarded: true,
            args: [
                {
                    key: 'text',
                    prompt: 'Text',
                    type: 'string',
                    default: ''
                },
            ]
        });
    }

    async run(msg, { text }) {
        const canvas = createCanvas(SIZE, SIZE);
        const ctx = canvas.getContext('2d');
        const encoder = new GIFEncoder(SIZE, SIZE);

        let gifWriteBuffer = new streamBuffers.WritableStreamBuffer({
            initialSize: (100 * 1024),   // start at 100 kilobytes.
            incrementAmount: (10 * 1024) // grow by 10 kilobytes each time buffer overflows.
        });

        encoder.createReadStream().pipe(gifWriteBuffer);
        encoder.start();
        encoder.setRepeat(0);
        encoder.setDelay(150);
        ctx.font = "32pt Consolas";

        const text_surface = ctx.measureText(text);
        const width = SIZE / 12;
        for (let scroll_pos = width + SIZE; scroll_pos > -text_surface.width - width; scroll_pos -= width) {
            ctx.fillStyle = "#36393F";
            ctx.fillRect(0, 0, SIZE, SIZE);
            ctx.fillStyle = "#00ff00";
            ctx.fillText(text, scroll_pos, 48); 
            encoder.addFrame(ctx);
        }
        encoder.finish();
        gifWriteBuffer.on('finish', () => {
            return msg.say({ files: [{ attachment: gifWriteBuffer.getContents(), name: 'scrolling-text.gif' }] });
        });
    }
};
