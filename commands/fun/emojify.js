const { Command } = require('discord.js-commando');
const emojiMap = require('../../resources/json/emojiMapping');
const SYMBOLS = '!"#$%&\'()*+,-./:;<=>?@[]^_`{|}~ \n\\';

module.exports = class HelpCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'emojify',
            group: 'fun',
            memberName: 'emojify',
            description: 'Emojify your text',
            guarded: true,
            args: [
                {
                    key: 'text',
                    prompt: 'Text to be emojified',
                    type: 'string',
                    default: ''
                },
            ]
        });
    }

    async run(msg, { text }) {
        msg.channel.send(this.wordArrayToEmojipasta(this.createWordArray(text)));
    }

    createWordArray(input) {
        let output = [];
        let currentWord = "";
        for (let i = 0; i < input.length; i++) {
            if (SYMBOLS.indexOf(input[i]) > -1) {
                if (currentWord !== "") {
                    output.push(currentWord);
                    currentWord = "";
                }
                output.push(input[i]);
            } else {
                currentWord += input[i];
            }
        }

        if (currentWord !== "") {
            output.push(currentWord);
        }
        
        return output;
    }

     wordArrayToEmojipasta(wordArray) {
        let emojiRepeatArray = this.getRandomIntArray(50);
        let output = "";
        let i = 0;
        while (i < wordArray.length) {
            let word = wordArray[i];

            let personalEmoji = emojiMap.personal[word.toLowerCase()];
            let emoji = emojiMap.regular[word.toLowerCase()];
            
            if (personalEmoji && typeof(personalEmoji) === "object") {
                let wordArraySliced = wordArray.slice(i, i + personalEmoji.length);
                let wordArraySlicedLowercase = wordArraySliced.filter(word => word.toLowerCase());
                if (this.arraysEqual(personalEmoji.wordArray, wordArraySlicedLowercase)) {
                    let totalWord = "";
                    for(let k = 0; k<wordArraySliced.length; k++){
                        totalWord += wordArraySliced[k];
                    }
                    output += totalWord + " " + personalEmoji.emoji;
                    i += personalEmoji.length - 1;
                } else {
                    output += word;
                }
            }
            else if (personalEmoji && typeof(personalEmoji) !== "object") {
                output += word;
                output += " " + personalEmoji;
            }
            else {
                output += word;

                if (emoji) {
                    let repeat = emojiRepeatArray[i % emojiRepeatArray.length];
                    let totalEmoji = "";
                    for (let j = 0; j < repeat; j++) {
                        totalEmoji += emoji;
                    }
                    output += " " + totalEmoji;
                }
            }
            i++;
        }
        
        return output;
    }

    getRandomIntArray(x) {
        let result = [];
        for (let i = 0; i < x; i++) {
            result.push(this.getRandomInt(1, 4));
        }
        
        return result;
    }

    getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }

    arraysEqual(a, b) {
        if (a === b) {
            return true;
        }
        if (a == null || b == null) {
            return false;
        }
        if (a.length !== b.length) {
            return false;
        }
        for (let i = 0; i < a.length; ++i) {
            if (a[i] !== b[i]) return false;
        }
        return true;
    }
};
