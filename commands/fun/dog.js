const { Command } = require('discord.js-commando');
const fetch = require('node-fetch');

module.exports = class HelpCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'dog',
            group: 'fun',
            memberName: 'dog',
            description: 'Shows a random picture of a dog',
            guarded: true,
        });
    }

    async run(msg) {
        try {
            let dogFactResponse = await fetch('https://some-random-api.ml/facts/cat');
            let dogFact = dogFactResponse.json().fact;
            let dogResponse = await fetch('https://dog.ceo/api/breeds/image/random');
            let dog = await fetch(dogResponse.json().message);
            let dogBuffer = await dog.buffer();

            return msg.say(dogFact, { files:  [{attachment: dogBuffer, name: 'dog.png'}]});
        } catch(ex) {
            console.error(ex);
        }
    }
};
