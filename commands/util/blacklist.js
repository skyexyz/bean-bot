const { Command } = require('discord.js-commando');
const fetch = require('node-fetch');

module.exports = class HelpCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'blacklist',
            group: 'util',
            memberName: 'blacklist',
            description: 'Add a word to the blacklist',
            guarded: true,
            clientPermissions: ['ADMINISTRATOR'],
	        userPermissions: ['MANAGE_MESSAGES'],
        });
    }

    async run(msg) {
        msg.client.provider.get('blacklist');
        try {
            let catFactResponse = await fetch('https://some-random-api.ml/facts/cat');
            let catFact = catFactResponse.json().fact;
            let cat = await fetch('https://cataas.com/cat');
            let catBuffer = await cat.buffer();

            return msg.say(catFact, { files:  [{attachment: catBuffer, name: 'cat.png'}]});
        } catch(ex) {
            console.error(ex);
        }
    }
};
